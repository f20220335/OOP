class Record extends AbstractRecord {
    private Object valueObject; // Concretising

    public Record(Object val) { // Concretising
        valueObject = val;
    }

    public Object key() { // Can you generalize or abstract this further away?

        if (valueObject instanceof Double) {
            return (Object) Math.round((double) value());
        } // An ad-hoc arrangement.
        else if(valueObject instanceof Integer)
        {return value();

        }
        else if(valueObject instanceof String)
        {String s;
        s=(String)valueObject;
        char c=s.charAt(0);
        return (Object)c;

        }
        return null;
    }

    public Object value() {
        return valueObject;
    }

    public Comparison compare(AbstractRecord another) {
        if(another.key() instanceof Integer&&key() instanceof Integer)
       { long k1 = (long) key(), k2 = (long) (another.key()); // The same ad-hoc arrangement
        boolean b1 = k1 <= k2, b2 = k1 >= k2; // All this should be evident
        if (b1 && b2)
            return Comparison.MATCHING;
        if (b1 && !b2)
            return Comparison.PREDECESSOR;
        if (!b1 && b2)
            return Comparison.SUCCESSOR;
       }
       else if(another.key() instanceof Character&&key() instanceof Character)
       {char k1 = (char) key(), k2 = (char) (another.key()); // The same ad-hoc arrangement
        boolean b1 = k1 <= k2, b2 = k1 >= k2; // All this should be evident
        if (b1 && b2)
            return Comparison.MATCHING;
        if (b1 && !b2)
            return Comparison.PREDECESSOR;
        if (!b1 && b2)
            return Comparison.SUCCESSOR;


       }
        return Comparison.INCOMPARABLE;
    }

    public void show() {
        System.out.print(" [Key: " + key() + "] [Value: " + value() + "] ");
    }
}
